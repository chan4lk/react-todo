import { getGreeting } from '../support/app.po';

describe('todo-app', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    // Function helper example, see `../support/app.po.ts` file
    getGreeting().contains('Welcome to todo-app!');
  });

  it('should add todo', () => {
    cy.get('input').type('Task 1');
    cy.get('button').click();
    cy.get('div').should('contain.text', 'Task 1');
  });
});
