describe('components: WelcomeMessage component', () => {
  beforeEach(() =>
    cy.visit(
      '/iframe.html?id=welcomemessage--primary&knob-message=test message'
    )
  );

  it('should render the component', () => {
    cy.get('h1').should('contain', 'Welcome to todo-app!');
  });

  it('should render the message', () => {
    cy.get('h2').should('contain', 'test message');
  });
});
