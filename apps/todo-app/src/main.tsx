import React from 'react';
import ReactDOM from 'react-dom';

import App from './app/app';

import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';

import { TODOS_FEATURE_KEY, todosReducer } from './app/+state/todos.slice';

const store = configureStore({
  reducer: { [TODOS_FEATURE_KEY]: todosReducer },
});

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
