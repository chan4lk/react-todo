import React from 'react';
import styled from '@emotion/styled';
import { WelcomeMessage, TodoList, AddTodo } from '@reactnx/components';
import { useSelector, useDispatch } from 'react-redux';
import { todosSelectors } from './+state/todos.selectors';
import { todosActions } from './+state/todos.actions';

const StyledApp = styled.div`
  margin: 0 auto;
  width: 700px;

  }
`;

export const App = () => {
  const todos = useSelector(todosSelectors.selectAll);
  const dispatch = useDispatch();
  const action = (title: string) =>
    dispatch(
      todosActions.addTodos({ id: todos.length + 1, title, completed: false })
    );
  return (
    <StyledApp>
      <WelcomeMessage message={'Add todos here'}></WelcomeMessage>
      <AddTodo dispatch={action} />
      <TodoList todos={todos} />
    </StyledApp>
  );
};

export default App;
