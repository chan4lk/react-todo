import React from 'react';
import { render } from '@react-nx/shared';

import App from './app';
import {
  initialTodosState,
  TODOS_FEATURE_KEY,
  todosReducer,
} from './+state/todos.slice';
import { configureStore } from '@reduxjs/toolkit';

describe('App', () => {
  let store;
  let initialState;
  beforeAll(() => {
    store = configureStore({
      reducer: { [TODOS_FEATURE_KEY]: todosReducer },
    });
    initialState = { [TODOS_FEATURE_KEY]: initialTodosState };
  });
  it('should render successfully', () => {
    const { baseElement } = render(<App />, {
      initialState,
      store,
    });

    expect(baseElement).toBeTruthy();
  });

  it('should have a greeting as the title', () => {
    const { getByText } = render(<App />, {
      initialState,
      store,
    });

    expect(getByText('Welcome to todo-app!')).toBeTruthy();
  });
});
