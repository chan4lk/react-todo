import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
  EntityState,
  PayloadAction,
} from '@reduxjs/toolkit';

export const TODOS_FEATURE_KEY = 'todos';

/*
 * Update these interfaces according to your requirements.
 */
export interface TodosEntity {
  id: number;
  title: string;
  completed: boolean;
}

export interface TodosState extends EntityState<TodosEntity> {
  loadingStatus: 'not loaded' | 'loading' | 'loaded' | 'error';
  error: string;
}

export const todosAdapter = createEntityAdapter<TodosEntity>();

/**
 * Export an effect using createAsyncThunk from
 * the Redux Toolkit: https://redux-toolkit.js.org/api/createAsyncThunk
 */
export const fetchTodoss = createAsyncThunk(
  'todos/fetchStatus',
  async (_, thunkAPI) => {
    /**
     * Replace this with your custom fetch call.
     * For example, `return myApi.getTodoss()`;
     * Right now we just return an empty array.
     */
    return Promise.resolve([]);
  }
);

export const initialTodosState: TodosState = todosAdapter.getInitialState({
  loadingStatus: 'not loaded',
  error: null,
});

export const todosSlice = createSlice({
  name: TODOS_FEATURE_KEY,
  initialState: initialTodosState,
  reducers: {
    addTodos: todosAdapter.addOne,
    removeTodos: todosAdapter.removeOne,
    // ...
  },
  extraReducers: (builder) => {
    builder.addCase(fetchTodoss.pending, (state: TodosState) => {
      state.loadingStatus = 'loading';
    });
    builder.addCase(
      fetchTodoss.fulfilled,
      (state: TodosState, action: PayloadAction<TodosEntity[]>) => {
        todosAdapter.addMany(state, action.payload);
        state.loadingStatus = 'loaded';
      }
    );
    builder.addCase(fetchTodoss.rejected, (state: TodosState, action) => {
      state.loadingStatus = 'error';
      state.error = action.error.message;
    });
  },
});

/*
 * Export reducer for store configuration.
 */
export const todosReducer = todosSlice.reducer;
