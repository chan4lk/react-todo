import { createSelector } from '@reduxjs/toolkit';
import { todosAdapter, TodosState, TODOS_FEATURE_KEY } from './todos.slice';
/*
 * Export selectors to query state. For use with the `useSelector` hook.
 *
 * e.g.
 * ```
 * const entities = useSelector(selectTodosEntities);
 * ```
 *
 * See: https://react-redux.js.org/next/api/hooks#useselector
 */
const defaultSelectors = todosAdapter.getSelectors();
const getTodosState = (rootState: unknown): TodosState =>
  rootState[TODOS_FEATURE_KEY];
const selectAll = createSelector(getTodosState, (state) =>
  defaultSelectors.selectAll(state)
);
export const todosSelectors = {
  getTodosState,
  selectAll,
};
