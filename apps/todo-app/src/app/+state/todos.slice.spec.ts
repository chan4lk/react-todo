import { todosAdapter, todosReducer } from './todos.slice';
import { todosActions } from './todos.actions';

describe('todos reducer', () => {
  it('should handle initial state', () => {
    const expected = todosAdapter.getInitialState({
      loadingStatus: 'not loaded',
      error: null,
    });

    expect(todosReducer(undefined, { type: '' })).toEqual(expected);
  });

  it('should handle fetchTodoss', () => {
    let state = todosReducer(
      undefined,
      todosActions.fetchTodoss.pending(null, null)
    );

    expect(state).toEqual(
      expect.objectContaining({
        loadingStatus: 'loading',
        error: null,
      })
    );

    state = todosReducer(
      state,
      todosActions.fetchTodoss.fulfilled([{ id: 1 }], null, null)
    );

    expect(state).toEqual(
      expect.objectContaining({
        loadingStatus: 'loaded',
        error: null,
        entities: { 1: { id: 1 } },
      })
    );

    state = todosReducer(
      state,
      todosActions.fetchTodoss.rejected(new Error('Uh oh'), null, null)
    );

    expect(state).toEqual(
      expect.objectContaining({
        loadingStatus: 'error',
        error: 'Uh oh',
        entities: { 1: { id: 1 } },
      })
    );
  });
});
