import { todosSlice, fetchTodoss } from './todos.slice';
/*
 * Export action creators to be dispatched. For use with the `useDispatch` hook.
 *
 * e.g.
 * ```
 * const dispatch = useDispatch();
 * dispatch(todosActions.addTodos([{ id: 1 }]));
 * ```
 *
 * See: https://react-redux.js.org/next/api/hooks#usedispatch
 */

export const todosActions = {
  ...todosSlice.actions,
  fetchTodoss,
};
