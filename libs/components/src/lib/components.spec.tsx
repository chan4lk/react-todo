import React from 'react';
import { render } from '@testing-library/react';

import { WelcomeMessage } from './components';

describe(' Components', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<WelcomeMessage message={'test message'} />);
    expect(baseElement).toBeTruthy();
  });
});
