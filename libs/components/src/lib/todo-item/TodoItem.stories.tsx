import React from 'react';
import { TodoItem, TodoItemProps } from './TodoItem';
import { withKnobs, text, boolean } from '@storybook/addon-knobs/react';
export default {
  component: TodoItem,
  title: 'Todo Item',
  decorators: [withKnobs],
};

export const primary = () => {
  /* eslint-disable-next-line */
  const props: TodoItemProps = {
    item: {
      completed: boolean('completed', 'true'),
      id: 1,
      title: text('title', 'Task 1'),
    },
  };

  return <TodoItem {...props} />;
};
