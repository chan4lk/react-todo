import React from 'react';
import { render } from '@testing-library/react';

import TodoItem, { TodoItemProps } from './TodoItem';

describe(' TodoItem', () => {
  it('should render successfully', () => {
    const props: TodoItemProps = {
      item: {
        id: 1,
        title: 'task 1',
        completed: false,
      },
    };
    const { baseElement } = render(<TodoItem {...props} />);
    expect(baseElement).toBeTruthy();
  });
});
