import React from 'react';

import styled from '@emotion/styled';

export interface TodoEntity {
  id: number;
  title: string;
  completed: boolean;
}

/* eslint-disable-next-line */
export interface TodoItemProps {
  item: TodoEntity;
}

const StyledTodoItem = styled.div`
  color: #0080ff;
  display: flex;
  font-size: 2rem;
  margin-top: 20px;
  border: 2px solid #eee;
  .number {
    flex: auto;
  }

  .title {
    flex: 2;
  }
`;

export const TodoItem = (props: TodoItemProps) => {
  return (
    <StyledTodoItem>
      <div className="number">{props.item.id}</div>
      <div className="title">{props.item.title}</div>
    </StyledTodoItem>
  );
};

export default TodoItem;
