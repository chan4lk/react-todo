import React from 'react';

import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface ComponentsProps {
  message: string;
}

const StyledComponents = styled.div`
  color: green;
  background-color: #eee;
`;

export const WelcomeMessage = (props: ComponentsProps) => {
  return (
    <StyledComponents>
      <h1>Welcome to todo-app!</h1>
      <h2>{props.message}</h2>
    </StyledComponents>
  );
};

export default WelcomeMessage;
