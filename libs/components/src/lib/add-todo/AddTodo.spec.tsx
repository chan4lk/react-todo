import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import AddTodo, { AddTodoProps } from './AddTodo';

describe(' AddTodo', () => {
  it('should render successfully', () => {
    const props: AddTodoProps = {
      dispatch: jest.fn(),
    };
    const { baseElement } = render(<AddTodo {...props} />);
    expect(baseElement).toBeTruthy();
  });

  it('should call dispatch successfully', () => {
    const props: AddTodoProps = {
      dispatch: jest.fn(),
    };
    const { getByTestId, getByText } = render(<AddTodo {...props} />);
    const input = getByTestId('task-input');
    fireEvent.change(input, {
      target: { value: 'Todo item 1' },
    });

    const button = getByText('Add');
    fireEvent.click(button);
    expect(props.dispatch).toHaveBeenCalled();
  });
});
