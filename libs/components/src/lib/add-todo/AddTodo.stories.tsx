import React from 'react';
import { AddTodo, AddTodoProps } from './AddTodo';
import { withKnobs } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
export default {
  component: AddTodo,
  title: 'Add Todo',
  decorators: [withKnobs],
};

export const primary = () => {
  /* eslint-disable-next-line */
  const props: AddTodoProps = {
    dispatch: action('dispatch'),
  };

  return <AddTodo {...props} />;
};
