import React, { useRef } from 'react';

import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface AddTodoProps {
  dispatch: (title: string) => void;
}

const StyledAddTodo = styled.div`
  color: pink;
  form {
    width: 100%;
    display: flex;
  }

  .input {
    flex: 2;
    font-size: 2rem;
  }

  .btn {
    flex: 1;
  }
`;

const StyledButton = styled.button`
  color: #a9a5f3;
  font-size: 1.5rem;
`;

export const AddTodo = (props: AddTodoProps) => {
  const taskRef = useRef(null);
  const onAdd = (e: any) => {
    const value = taskRef.current.value;
    if (value) {
      props.dispatch(value);
      taskRef.current.value = '';
    }
    e.preventDefault();
    taskRef.current.focus();
    return false;
  };

  return (
    <StyledAddTodo>
      <form onSubmit={onAdd}>
        <input
          data-testid="task-input"
          className="input"
          ref={taskRef}
          type="text"
        />
        <StyledButton className="btn" onClick={onAdd}>
          Add
        </StyledButton>
      </form>
    </StyledAddTodo>
  );
};

export default AddTodo;
