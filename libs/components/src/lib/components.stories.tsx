import React from 'react';
import { WelcomeMessage, ComponentsProps } from './components';
import { withKnobs, text } from '@storybook/addon-knobs/react';
export default {
  component: WelcomeMessage,
  title: 'WelcomeMessage',
  decorators: [withKnobs],
};

export const primary = () => {
  /* eslint-disable-next-line */
  const props: ComponentsProps = {
    message: text('message', 'story book message'),
  };

  return <WelcomeMessage {...props} />;
};
