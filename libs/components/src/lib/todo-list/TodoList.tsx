import React from 'react';

import styled from '@emotion/styled';
import { TodoItem, TodoEntity } from '../todo-item/TodoItem';

/* eslint-disable-next-line */
export interface TodoListProps {
  todos: TodoEntity[];
}

const StyledTodoList = styled.div`
  color: pink;
`;

export const TodoList = (props: TodoListProps) => {
  return (
    <StyledTodoList>
      {props.todos.map((todo) => {
        return <TodoItem key={todo.id} item={todo} />;
      })}
    </StyledTodoList>
  );
};

export default TodoList;
