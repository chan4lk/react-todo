import React from 'react';
import { TodoList, TodoListProps } from './TodoList';
import { withKnobs, text, boolean } from '@storybook/addon-knobs/react';
export default {
  component: TodoList,
  title: 'Todo List',
  decorators: [withKnobs],
};

export const primary = () => {
  /* eslint-disable-next-line */
  const props: TodoListProps = {
    todos: [
      {
        completed: boolean('completed', 'true'),
        id: 1,
        title: text('title', 'Task 1'),
      },
      {
        completed: boolean('completed', 'true'),
        id: 2,
        title: text('title2', 'Task 2'),
      },
      {
        completed: boolean('completed', 'true'),
        id: 3,
        title: text('title3', 'Task 3'),
      },
    ],
  };

  return <TodoList {...props} />;
};
