import React from 'react';
import { render } from '@testing-library/react';

import TodoList, { TodoListProps } from './TodoList';

describe(' TodoList', () => {
  it('should render successfully', () => {
    const props: TodoListProps = {
      todos: [],
    };
    const { baseElement } = render(<TodoList {...props} />);
    expect(baseElement).toBeTruthy();
  });
});
